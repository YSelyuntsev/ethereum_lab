# Лабораторная работа №5

### Разработка децентрализованных приложений на платформе Ethereum

Децентрализованные приложения – это особая разновидность интернет-приложений, основанных на одноранговой сети.

Ethereum – это децентрализованная платформа, поверх которой можно разворачивать децентрализованные приложения. Смарт-контракт – компьютерный алгоритм, предназначенный для заключения и поддержания самоисполняемых контрактов, выполняемых в блокчейн-среде. Смарт-контракты для платформы Ethereum могут быть написаны на различных языках программирования, Solidity, LLL и Serpent.

## 1. Настройка сети Ethereum

Первым делом устанавливаем 2 пакета для развертывания blockchain сети

```
npm i -g ganache-cli
```

```
npm i -g truffle
```

![Step 1](screenshots/1.png)
![Step 2](screenshots/2.png)

После установки пакета разворачиваем тестовую blockchain rpc сеть командами:

```
ganache-cli
```

или

```
testrpc
```

Результатом запуска blockchain сети будет следующий вывод в консоль:

![Step 3](screenshots/6.png)

## 2. Разработка и деплой смарт-контракта

Для дальнейшей работы воспользуемся ранее установленным пакетом `truffle`.

Инициализируем новый проект Ethereum командой:

```
truffle init
```

![Step 4](screenshots/4.png)

Эта команда создаст базовую структуру из папок:

- contracts – каталог, в котором Truffle ожидает найти контракты на языке Solidity;
- migrations – каталог для хранения файлов, содержащих код развертывания контрактов;
- test – место для тестовых файлов, применяемых при тестировании ваших смарт-контрактов;
- truffle.js – главный файл конфигурации Truffle;

Для начала нужно провести конфигурацию. В файле `truffle-config.js` расскоментировать следующий участок кода, отвечающий за подключение к тестовой сети blockchain.

![Step 4.1](screenshots/8.png)

Далее нам необходимо скомпилировать смартконтракт и выполнить миграции, выполнив следующие команды:

```
truffle compile
```

![Step 5](screenshots/5.png)

```
truffle migrate
```

![Step 6](screenshots/7.png)

## 3. Взаимодействие со смарт-контрактом. Просмотр доступных методов

Для взаимодействия с сетью блокчейна на уровне протокола RPC использовалась библиотека web3.js, которая предоставляет API сети блокчейн.

Установим пакет `web3.js` командой:

```
npm i web3
```

Создадим новый файл `app.js` в корне с проектом, в котором инициализируем библиотеку `web3.js`, выполним подключение, а также выведем в консоль все поля и методы у объекта `eth`, который предназначен для работы с Ethereum.

![Step 7](screenshots/9.png)

Запустим написанную программу с помощью команды:

```
node app.js
```

Результатом будет следующий вывод в консоль:

![Step 8](screenshots/10.png)
![Step 9](screenshots/11.png)
![Step 10](screenshots/12.png)
![Step 11](screenshots/13.png)
![Step 12](screenshots/14.png)
![Step 13](screenshots/15.png)
![Step 14](screenshots/16.png)
![Step 15](screenshots/17.png)
![Step 16](screenshots/18.png)
![Step 17](screenshots/19.png)
![Step 18](screenshots/20.png)
